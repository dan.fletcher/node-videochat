var fs = require('fs');
var express = require('express');
var serveStatic = require('serve-static');
var path = require('path');
var morgan = require('morgan');
var healthChecker = require('sc-framework-health-check');
var msg = require('./msg');

module.exports.run = function (worker) {
  console.log('   >> Worker PID:', process.pid);
  var environment = worker.options.environment;

  var app = express();

  var httpServer = worker.httpServer;
  var scServer = worker.scServer;

  if (environment == 'dev') {
    // Log every HTTP request. See https://github.com/expressjs/morgan for other
    // available formats.
    app.use(morgan('dev'));
  }
  app.use(serveStatic(path.resolve(__dirname, 'public')));

  // Add GET /health-check express route
  healthChecker.attach(worker, app);

  // here we start our database connector
  var mysql   = require('mysql');
  var sqlpool = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : 'dan',
    database : 'callcentre'
  });

  // we need to define a queue array, its quicker to use memory that sqldb
  var queue = [];
  var agent_list = [];
  var errorlog = true;

  httpServer.on('request', app);

  // In here we handle our incoming realtime connections and listen for events.
  scServer.on('connection', function (socket) {
    //console.log(socket)
  // Get a db connection from the pool
  sqlpool.getConnection(function(err, dbconn) {

    // pull some fields out of the http headers
    skt    = socket.id;
    ip     = socket.remoteAddress;
    domain = socket.request.headers.host;
    //url    = socket.request.headers.origin;
    url  = socket.request.url;

    // orgnise the fields to stick in the connection table
    values = '"'+skt+'","'+ip+'","'+domain+'","'+url+'"';
    // structure the insert statement
    sqlquery = 'INSERT INTO connections (socket, ip, domain, url) VALUES ('+values+')';
    // run the sql query
    dbconn.query(sqlquery, function(err, rows, fields) {

      if (err) console.log('Error doing '+sqlquery);
    });
    // Release connection to the pool
    dbconn.release();
    // Handle error after the release.
    if (err) throw error;
  });

  socket.on('clientv1', function (data) {
    // Get a db connection from the pool
    sqlpool.getConnection(function(err, dbconn) {
      if (errorlog) {
        console.log("Client > Server : "+socket.id+" : "+JSON.stringify(data));
      }
      msg.client(socket, data, dbconn, queue, agent_list, scServer);
      // Release connection to the pool
      dbconn.release();
      // Handle error after the release.
      if (err) throw error;
    });
  });


  socket.on('agentv1', function (data) {
    // Get a db connection from the pool
    sqlpool.getConnection(function(err, dbconn) {
      console.log("Agent  > "+socket.id+" > "+JSON.stringify(data));
      msg.agent(socket, data, dbconn, queue, agent_list, scServer);
      // Release connection to the pool
      dbconn.release();
      // Handle error after the release.
      if (err) throw error;
    });
  });

  socket.on('supervisorv1', function (data) {
    // Get a db connection from the pool
    sqlpool.getConnection(function(err, dbconn) {
      console.log("Supervisor message from "+socket.id+" > "+data);
      msg.supervisor(socket, data, dbconn, queue, agent_list);
      // Release connection to the pool
      dbconn.release();
      // Handle error after the release.
      if (err) throw error;
    });
  });

/*
    var interval = setInterval(function () {
      socket.emit('rand', {
        rand: Math.floor(Math.random() * 5)
      });
    }, 1000);
*/
    socket.on('disconnect', function () {
      // Get a db connection from the pool
      sqlpool.getConnection(function(err, dbconn) {
        console.log("Disconnect  > "+socket.id);
        var data = {
          type: "logout",
        };
        msg.agent(socket, data, dbconn, queue, agent_list, scServer);
        msg.client(socket, data, dbconn, queue, agent_list, scServer);
        // Release connection to the pool
        dbconn.release();
        // Handle error after the release.
        if (err) throw error;
      });
    });
  });
};
