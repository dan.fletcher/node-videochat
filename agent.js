var queuestatus = require('./queuestatus');

function defaultm(socket)
{
  console.log('defaultscript')
  // we need to report back to the client
  send = {
    type: 'failed',
    code: "msg_unknown",
    text: "This message was not found send json {type: logout / available / answer / text / sdp / candidate / dnd}",
  };
  socket.emit('agentv1', send, function(err){
    console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
  });
            console.log('enddefault');
}

function default_login(socket)
{
  send = {
    type: 'failed',
    code: "login_required",
    text: "you have not authorised first send json {type: login, username: abc, password: abc}"
  };
  socket.emit('agentv1', send, function(err){
    console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
  });
}

// this function will send answer message to the agent and set call as in session
function answer(socket, queue, agent_list, scServer, answertype)
{
  // update the client status to connected
  queue.forEach(function(queued, index) {
    if (queued.agentwebsocket == socket.id)
    {
        queued.status = "connected";
        clientwebsocket = queued.websocket
    }
  });

  // update the agent status to connected
  agent_list.forEach(function(agent) {
    if (agent.websocket == socket.id)
    {
        agent.status = "connected";
    }
  });

  // Update the wallboard
  send = {
    type: 'update',
    answertype: answertype,
    websocket: clientwebsocket,
    status: "connected"
  };
  console.log(JSON.stringify(send));
  scServer.exchange.publish("wallboard", send, function(err){
    console.log('Failed to send '+send+' to '+socket.id);
  });
}

// this function logs the user into a queue
function login(socket, channel, dbconn, agent_list, username, password, scServer)
{
  // structure the query statement
  sqlquery = 'SELECT FirstName, LastName, Chatname FROM agents where email=\''+username+'\' and password=\''+password+'\'';
  // run the sql query
  dbconn.query(sqlquery, function(err, rows, fields) {
    if (rows.length > 0 )
    {
      // if they dont already appear to be logged in
      send = {
        type: "loginok",
        text: "You have been sucessfully logged in",
        name: rows[0].FirstName,
        chatname: rows[0].Chatname,
      };
      socket.emit('agentv1', send, function(err){
        console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
      });

      // set marker has having authenticated
      socket.auth = true;

      agent_list.push({
        type: "agent",
        name: rows[0].FirstName +' '+rows[0].LastName,
        chatname: rows[0].Chatname,
        websocket: socket.id,
        datachannel: channel,
        timestamp: "",
        priority: 10,
        status: "dnd",
      });
      console.log('name '+rows[0].FirstName+' channel'+channel);
    }
    else
    {
      // we need to report a failure back to the client
      send = {
        type: 'failed',
        code: 'loginfailed',
        text: 'You were unable to login, please check your username and password',
      };
      socket.emit('agentv1', send, function(err){
        console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
      });
    }
    if (err) console.log('Error doing '+sqlquery);
  });
}

// Make the agent available
function available(socket, queue, agent_list, scServer)
{
  agent_list.forEach(function(agent) {
    if (agent.websocket == socket.id)
    {
      agent.status = 'available';
    }
  });
  // now available search for call
  next(socket, queue, agent_list, scServer);
}


// Make the agent unavailable
function unavailable(socket, queue, agent_list, scServer)
{
  console.log(agent_list)
  console.log(queue)
  // set the agent stats to DND
  agent_list.forEach(function(agent) {
    if (agent.websocket == socket.id)
    {
      agent.status = 'dnd';
    }
  });

  // update the client status to connected
  queue.forEach(function(queued) {
    if (queued.agentwebsocket == socket.id)
    {
        queued.status = 'queued';
        clientwebsocket = queued.websocket;
        queued.agentwebsocket = '';
        queued.connectedto = '';
      // Update the wallboard
      send = {
        type: 'update',
        websocket: clientwebsocket,
        status: 'queued'
      };
      console.log(JSON.stringify(send));
      scServer.exchange.publish("wallboard", send, function(err){
        console.log('Failed to send '+send+' to '+socket.id);
      }); 
    }
  });
  console.log(agent_list)
  console.log(queue)
}

// this function will send the next queued caller to the agent
function next(socket, queue, agent_list, scServer)
{
  // lookup all itmes in the queue
  var client_id = null;
  var priority = "";

  // search through the queue array where status is queued
  for(var i=0; i<queue.length; i++)
    {
      if (queue[i].status == "queued")
        {
          // always choose one with a higher priority
          if (queue[i].priority > priority)
          {
            priority = queue[i].priority;
            client_id = i;
          }
        }
      }
      console.log("find is "+client_id);

    // so if we find a call
    if (client_id !== null)
    {
      console.log("found something in the queue")

      // update the agent status to ringing
      agent_list.forEach(function(agent) {
        if (agent.websocket == socket.id)
        {
            agent.status = 'ringing';
	          agent_datachannel = agent.datachannel;
            agent_name = agent.name;
            agent_chatname = agent.chatname;
        }

      // update the queue status to ringing
      queue[client_id].agentwebsocket = socket.id
      queue[client_id].status = 'ringing';
      queue[client_id].connectedto = agent_chatname;
      });

      // Tell the agent about the call they have a call
      send = {
        type: 'newcall',
        code: queue[client_id].type,
        datachannel: queue[client_id].datachannel,
        text: "You have a new call to answer"
      };
      console.log(JSON.stringify(send));
      scServer.exchange.publish(agent_datachannel, send, function(err){
        console.log('Failed to send '+send+' to '+socket.id);
      });
      // Update the wallboard
      send = {
        type: 'update',
        websocket: queue[client_id].websocket,
        status: "ringing",
        connectedto: agent_name,
      };
      console.log(JSON.stringify(send));
      scServer.exchange.publish("wallboard", send, function(err){
        console.log('Failed to send '+send+' to '+socket.id);
      });
      }
      // else there is no call to send you
    else
    {
        send = {
          type: 'success',
          code: 'nocalls',
          text: 'Tried to send you a call but there doesnt appear to be any for you'
        };
        socket.emit('agentv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
        });
      }
}


// this function will cancel the call from the queue
function cancel(socket, dbconn, queue, agent_list, scServer){
  var find = 0;

  // removed the queued entry
  queue.forEach(function(queued, index) {
    if (queued.agentwebsocket == socket.id)
    {
        clientwebsocket = queued.websocket
        queue.splice(index,1);
        find = 1;
    }
    if (queued.status == 'queued'){
      send = {
        type: "queueupdate",
        pos: index,
        text: "Your new postion in the queue",
      };
      scServer.exchange.publish(queued.datachannel, send, function(err){
        console.log('Failed to send '+JSON.stringify(send)+' to '+queued.websocket);
      });
    }
    // Update the wallboard
    send = {
      type: 'cancel',
      websocket: clientwebsocket,
    };
    console.log(JSON.stringify(send));
    scServer.exchange.publish("wallboard", send, function(err){
      console.log('Failed to send '+send+' to '+socket.id);
    });
  });


    // update the agent status to available
    agent_list.forEach(function(agent) {
      if (agent.websocket == socket.id)
      {
          agent.status = 'available';
          agent_datachannel = agent.datachannel;
      }
    });

  switch (find)
    {
      case 1:
        send = {
          type: 'success',
          code: "cancelqueue",
          text: "successfully removed you from the queue",
        };
        socket.emit('agentv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
        });
      break;

      case 0:
        send = {
          type: 'failed',
          code: "notinqueue",
          text: "There was no request found in the queue to remove",
        };
        socket.emit('agentv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
        });
      break;
    }
    // find the next call
    next(socket, queue, agent_list, scServer)
}


function logout(socket, dbconn, queue, agent_list, scServer)
{
  // requeue any call
  queue.forEach(function(queued, index) {
    if (queued.agentwebsocket == socket.id)
      {
        queued.status = 'queued';
        queued.agentwebsocket = '';
      }
    });

    // set marker has having not authenticated
    socket.auth = null;

    // msg client to say they been requeued

    // completely delete the agent
    agent_list.forEach(function(agent, index) {
      if (agent.websocket == socket.id)
      {
        agent_list.splice(index,1);
      }
    });

    console.log(agent_list);
}


module.exports =
{
  logout: logout,
  available: available,
  unavailable: unavailable,
  default: defaultm,
  cancel: cancel,
  login: login,
  answer: answer,
  default_login: default_login,
};

var errorlog = true;
