// This little submodule will handle updating a queue system
// This will be used for call logging purposes and wallboard etc

// there will be a queuestatus subscription

module.exports =
{
  find: find,
  add: add,
  update: update,
//  assign: assign,
//  answer: answer,
//  requeue: requeue,
//  end: end,
};

var errorlog = true;


// A new request that comes in
function add(queue, type, socket, datachannel, url, timestamp, priority, status){
// {action:add,type:,channel:,url:,timestamp:,priority:,status}

}

function find(queue, socket){
  queue.forEach(function(queued, index) {
    if (queued.websocket == socket){
        return index;
    };
  });
}


function update(queue, socket, type){
  // set a find value to false
  found = false ;

  // first find the exiting item in the queue
  queue.forEach(function(queued, index) {
    if (queued.websocket == socket && queued.status == 'queued')
    {
        // now we can update the queued item
        queued.type = type,
        found = true;

        // we can report back to the client
        send = {
          type: 'success',
          code: "queueupdate",
          text: "Your request was found and update to "+requesttype,
        };

        socket.emit('clientv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket);
        });

        // We can update the queuestatus subscription
        send = {
          action: 'update',
          type: queued.type,
          websocket: queued.websocket,
          datachannel: queued.datachannel,
          url: queued.url,
          timestamp: queued.timestamp,
          priority: queued.priority,
          status: queued.status,
        };
        scServer.exchange.publish('queuestatus', send);

        // do logging here
        if (errorlog){
            console.log('Server > Agent :'+queued.websocket+' : '+JSON.stringify(send));
        }
    }
  });

  // if we don't find any results (which we shouldn't)
  if (!found){
    // we can report back to the client
    send = {
      type: "failed",
      code: "queue_notqueued",
      text: "Queue "+requesttype+" update failed, as you are not waiting in queue",
    };

    socket.emit('clientv1', send, function(err){
      console.log('Failed to send '+JSON.stringify(send)+' to '+socket);
    });
  };
}


// {action:del,channel}
// {action:assign,channel:,agnetwebsocket:,connectedto}
// {action:answer,channel:}
// {action:requeue,channel:}
// {action:end,channel}
