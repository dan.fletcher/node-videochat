To Use
======
To start the system run nodejs server.js

then in your web browser goto 
https://localhost:8000/client.html
https://localhost:8000/agent.html
https://localhost:8000/wallboard.htm




Issues
======

* I have not given you the database schema in the repo, so you won't be able to authenticate at the moment.
* All that output that says 'failed to send' no noticable problems though


ToDo
====
* Find out what you can do with a media proxy
* Connect to asterisk/sip phone
* Send a call to a manager
* Let a manager listen to calls
* Record calls
* Media affects on the stream (just for learning)
* Video/Audio recording without media proxy
* redo in vue.js and NodeJS API
* Send Current client URL on page open
* Transfer call
* Incorporate chat teams
* Introduce the concept multiple websites & priorities
* Design a message replay for agents/managers/clients reconnecting (maybe kafka)
* A continious ping between agent/client so both know if the other disappears
* Requeue, allow to put someone back in the queue


General Features
================
* Supports Client (users) and agents (responders)
* Supports Call Queues
* Supports Voice/video/text in one application



Client Features
===============
* Options to initiate video/voice or text chat
* User Tracking Tokens (db logging)
* URL tracking (db logging)
* Session reconnect to support browsing pages or accidental close
* Mirrors client session across mutiple windows/tabs
* Queue position notification


Agent Features
==============
* Supports Agent Logins
* Do not disturb
* Option to answer session downgraded video>voice>text
* A Wallboard of agents and clients queuing


How it works
============
The client generates a random data socket for chat subscribes to messages.
The client connects to the server and establishes a websocket to communicate with the server. Messages for the server, requestvideochat, requestvoicechat, requesttextchat, cancel; Are send down the  websocket as these are server processed Messages.
When an agent accepts a call, the client receives a message on the listening socket and sends messages to the agent to initiate a webrtc or just chat messages are sent over this socket.
