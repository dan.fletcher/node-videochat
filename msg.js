
var client = require('./client');
var agent = require('./agent');
//var supervisor = require('./supervisor');

// in this function we are processing the messages received from the client
var msg_client = function(socket, data, dbconn, queue, agent_list, scServer){

  // we will start a big switch for the request types
  switch (data.type)
    {
      // videochat means that a video chat session has been requested
      case "videochat":
        client.chat(data.type, data.channel, socket, dbconn, queue, agent_list, scServer);
      break;

      case "voicechat":
        client.chat(data.type, data.channel, socket, dbconn, queue, agent_list, scServer);
      break;

      case "textchat":
        client.chat(data.type, data.channel, socket, dbconn, queue, agent_list, scServer);
      break;

      case "cancel":
        client.cancel(socket, dbconn, queue, agent_list, scServer);
      break;

      case "logout":
        client.cancel(socket, dbconn, queue, agent_list, scServer);
      break;

      default:
        client.default(socket);
      break;
    }
};

var msg_agent = function(socket, data, dbconn, queue, agent_list, scServer){

  // we first check if we are authenticated
  if (socket.auth != null)
    {
    // we will start a big switch for the request types
    switch (data.type)
      {
        case "logout":
          agent.logout(socket, dbconn, queue, agent_list, scServer);
        break;

        case "available":
          agent.available(socket, queue, agent_list, scServer);
        break;

	      case "unavailable":
	        agent.unavailable(socket, queue, agent_list, scServer);
	      break;

        case "cancel":
          agent.cancel(socket, dbconn, queue, agent_list, scServer);
        break;

        case "answer":
          agent.answer(socket, queue, agent_list, scServer, data.answertype);
        break;

        default:
          agent.default(socket);
        break;
      }
    }
  else
    {
      // if your not authnticated check if this is auth request
      switch (data.type)
        {
          // if this was a login request we can process
          case "login":
          // extract the username and password
          username = data.username;
          password = data.password;
            agent.login(socket, data.channel, dbconn, agent_list, username, password, scServer);
          break;

          default:
            agent.default_login(socket)
          break;
        }
    }
};

// define the exports
module.exports =
{
  agent: msg_agent,
  client: msg_client,
  //supervisor: supervisor,
};
