var queueupdate = require('./queue');

function defaultm(socket)
{
  // we need to report back to the client
  send = {
    type: 'failed',
    code: "msg_unknown",
    text: "This message was not found send json {type: videochat / voicechat / textchat / cancel}",
  };

  socket.emit('clientv1', send, function(err){
    console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
  });
}

// this function will add an item to the queue & update if already there
function chat(requesttype, datachannel, socket, dbconn, queue, agent_list, scServer){
  // we need to know if an existion request already exists
  var find = "0";
  // search through the queue array where client.conn number exists already
  for(var i=0; i<queue.length; i++)
    {
      if (queue[i].websocket == socket.id)
        {
          // We can only modify queued calls
          if (queue[i].status == "queued"){
            queuestatus.update(queue, socket.id, requesttype)
          }
          else
          {
            // if they appear to alredy be talking we must error and not update
            // we need to report back to the client
            send = {
              type: "failed",
              code: "queue_notqueued",
              text: "Queue "+requesttype+" update failed, as you are not waiting in queue",
            };

            socket.emit('clientv1', send, function(err){
              console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
            });
          }
          find = 1;
        }
    }

    // if we do not find anything add a new entry to the queue
    if (find == 0)
    {
      var queueid = queue.push({
        type: requesttype,
        websocket: socket.id,
        datachannel: datachannel,
        url: socket.request.headers.host,
        timestamp: "",
        priority: 10,
        status: "queued", });

      // we need to report back to the client
      send = {
        type: 'success',
        code: "addedtoqueue",
        text: "successfully added a "+requesttype+" request to the queue",
      };
      socket.emit('clientv1', send, function(err){
        console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
      });

      send = {
        type: "queueupdate",
        pos: queueid,
        text: "Your new postion in the queue",
      };
      socket.emit('clientv1', send, function(err){
        console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
      });
    }
    console.log('Queue > '+JSON.stringify(queue));

    // Update the wallboard
    send = {
      type: requesttype,
      websocket: socket.id,
      datachannel: datachannel,
      url: socket.request.headers.host,
      timestamp: "",
      priority: 10,
      status: "queued",
      position: queueid,
    };
    console.log(JSON.stringify(send));
    scServer.exchange.publish("wallboard", send, function(err){
      console.log('Failed to send '+send+' to '+socket.id);
    });

    // now available search for call
    next(socket, queue, agent_list, scServer);
}

// this function will check if there are any agents to send the call to
function next(socket, queue, agent_list, scServer)
{
  // lookup all itmes in the queue
  var agent_id = null;
  var priority = "";

  // search through the queue array where status is queued
  for(var i=0; i<agent_list.length; i++)
    {
      if (agent_list[i].status == "available")
        {
          agent_list[i].status = 'ringing';
          agent_id = agent_list[i].websocket;
          agent_name = agent_list[i].name;
          agent_chatname = agent_list[i].chatname;
          agent_datachannel = agent_list[i].datachannel;
        }
    }

    // so if we find a call
    if (agent_id !== null)
    {
      // update the agent status to ringing
      queue.forEach(function(queue) {
        if (queue.websocket == socket.id)
        {
            queue.status = 'ringing';
            queue.connectedto = agent_chatname;
            queue.agentwebsocket = agent_id;
            client_type = queue.type;
            client_datachannel = queue.datachannel;
        }

        // Tell the agent about the call they have a call
        send = {
          type: 'newcall',
          code: client_type,
          datachannel: client_datachannel,
          text: "You have a new call to answer"
        };

        console.log(JSON.stringify(send));
        scServer.exchange.publish(agent_datachannel, send, function(err){
          console.log('Failed to send '+send+' to '+agent_id);
        });

        // Update the wallboard
        send = {
          type: 'update',
          websocket: socket.id,
          status: "ringing",
          connectedto: agent_name,
        };
        console.log(JSON.stringify(send));
        scServer.exchange.publish("wallboard", send, function(err){
          console.log('Failed to send '+send+' to '+socket.id);
        });
      });
    }
    // else there is no call to send you
    else
    {
      send = {
          type: 'success',
          code: 'busy',
          text: 'Tried to find an agent but none appear to be free'
        };
        socket.emit('agentv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
        });
    }
}


// this function will remove all items for the client.conn
function cancel(socket, dbconn, queue, agent_list, scServer){
  // need to know if something was found for reporting
  var find = 0;
  // search the queue for
  for(var i=0; i<queue.length; i++)
    {
      if (queue[i].websocket == socket.id)
        {
          queue.splice(i,1);
          find = 1;
        }
    }
  switch (find)
    {
      case 1:
        send = {
          type: 'success',
          code: "cancelqueue",
          text: "successfully removed you from the queue",
        };
        socket.emit('clientv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
        });

        // Update the wallboard
        send = {
          type: 'cancel',
          websocket: socket.id,
        };
        console.log(JSON.stringify(send));
        scServer.exchange.publish("wallboard", send, function(err){
          console.log('Failed to send '+send+' to '+socket.id);
        });

      break;

      case 0:
        send = {
          type: 'failed',
          code: "notinqueue",
          text: "There was no request found in the queue to remove",
        };
        socket.emit('clientv1', send, function(err){
          console.log('Failed to send '+JSON.stringify(send)+' to '+socket.id);
        });
      break;
    }
}

function logout(socket, dbconn, queue, agent_list)
{
  // delete the call
  queue.forEach(function(queued, index) {
    if (queued.agentwebsocket == socket.id)
      {
        queue.splice(index,1);
      }
    });

    // msg agent to say call ended

    // completely delete the agent
    agent_list.forEach(function(agent, index) {
      if (agent.websocket == socket.id)
      {
        agent.status = 'available';
      }
    });
}

module.exports =
{
  chat: chat,
  cancel: cancel,
  default: defaultm,
  logout: logout,
};
